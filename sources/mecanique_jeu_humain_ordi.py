import ttt as jeu
from random import randint


def initialisation():    
    return randint(1,2)

def message_joueur(joueur):
    return "A vous de jouer, joueur"+ str(joueur) + " : "

def changer_joueur(joueur):
    if joueur == 1:
        joueur = 2
    else:
        joueur = 1
    return joueur
    
def boucle_jeu(joueur, situation):
    #joueur_courant = joueur
    coups_possibles = jeu.coups_possibles(situation)
    print(message_joueur(joueur))
    if joueur == 2:
        coup = jeu.coup_ordi(coups_possibles)
    else:
        coup = jeu.saisie_coup(coups_possibles)
    situation = jeu.changer_situation(coup,situation,joueur)
    #jeu.tester_fin(coup,situation) 
    if jeu.tester_fin(situation):  #teste si la liste est vide ou pas
        #resultat(coup,situation)
        jeu.fin_jeu(situation,joueur)
        print(jeu.afficher_jeu(situation))
    else:
        print(jeu.afficher_jeu(situation))
        joueur = changer_joueur(joueur)
        boucle_jeu(joueur,situation)

if __name__ == '__main__':
    joueur_courant = initialisation()
    print(jeu.afficher_jeu(jeu.initialisation_liste()))
    boucle_jeu(joueur_courant,jeu.initialisation_liste())
    
