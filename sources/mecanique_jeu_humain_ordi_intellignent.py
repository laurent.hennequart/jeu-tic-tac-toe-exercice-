import nim as jeu
from random import randint


def initialisation():    
    return randint(1,2)

def message_joueur(joueur):
    return "A vous de jouer, joueur"+ str(joueur) + " : "

def changer_joueur(joueur):
    if joueur == 1:
        joueur = 2
    else:
        joueur = 1
    return joueur

def coeff(joueur):
    if joueur == 2:
        return 1
    else:
        return -1

def min_max(situation,joueur):
    if jeu.tester_fin(situation):
        return jeu.evaluation_minmax(situation)*coeff(joueur), 0
    else:
        situSuivantes = [jeu.changer_situation(k,situation) for k in jeu.coups_possibles(situation)]
        if joueur == 2:
            valeur = max( [ min_max(suivante,1)[0]  for suivante in situSuivantes ] )
            indice = [ min_max(suivante,1)[0]  for suivante in situSuivantes ].index(valeur)
        else: # joueur = ADVERSAIRE
            valeur = min( [ min_max(suivante, 2)[0] for suivante in situSuivantes ] )
            indice = [ min_max(suivante, 2)[0] for suivante in situSuivantes ].index(valeur) 
    return valeur, indice
   

    
def boucle_jeu(joueur, situation):
    #joueur_courant = joueur
    
    coups_possibles = jeu.coups_possibles(situation)
    print(message_joueur(joueur))
    if joueur == 2:
        coup = coups_possibles[min_max(situation, joueur)[1]]  
         
    else:
        coup = jeu.saisie_coup(coups_possibles)
    situation = jeu.changer_situation(coup,situation)
    #jeu.tester_fin(coup,situation) 
    if jeu.tester_fin(situation):  #teste si la liste est vide ou pas
        #resultat(coup,situation)
        print("joueur"+ str(joueur) + " a perdu")
    else:
        print(jeu.afficher_jeu(situation))
        joueur = changer_joueur(joueur)
        boucle_jeu(joueur,situation)

if __name__ == '__main__':
    joueur_courant = initialisation()
    print(jeu.afficher_jeu(jeu.initialisation_liste()))
    boucle_jeu(joueur_courant,jeu.initialisation_liste())
    

