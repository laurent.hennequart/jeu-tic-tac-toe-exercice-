from random import randint

#création de la grille 3x3 initiale
def initialisation_liste():
    '''
    :return liste avec les 9 valeurs initialisées à 0
    '''
    return [[0,0,0],
            [0,0,0],
            [0,0,0]
            ]
#affichage du jeu
def afficher_jeu(liste):
    '''
    superbe affichage du jeu
    :param liste contenant les situations du jeu
    :return une grille avec les pions B et N
    '''
    for k in range(3):
        print(' ',end='')
        for i in range(3):
            print('--',end='')
        print('-')
        print(k+1,end='')
        for i in range(3):
            print('|',end='')
            if liste[k][i]==0:
                print(' ',end='')
            elif liste[k][i]==1:
                print('X',end='')
            else:
                print('O',end='')
        print('|')
    print(' ', end='')
    for i in range(3):
        print('--',end='')
    print('-')
    print('  ',end='')
    for i in range(3):
        print(i+1, end=' ')

#propose les coups possibles
def saisie_coup(liste):
    '''
    permet de saisir les valeurs horizontales et verticales du joueur sachant que celle-ci doit être inférieure aux dimensions de la grille
    :param liste liste contenant les différentes possibilités du joueur courant
    :return liste des deux valeurs
    '''
    flag=False
    while flag==False:
        row=int(input())
        col=int(input())
        if [row-1,col-1] in liste:
            flag=True
        else:
            flag=False
    return [row-1,col-1]

#
def coups_possibles(situation):
    '''
    :param situation contenant la grille
    :return une liste avec emplacements dispos
    '''
    coups = []
    for i in range(3):
        for j in range(3):
            if situation[i][j] == 0:
                coups.append([i,j])
    return coups

#retourne la liste des coups possibles suivant le nombre restant
def changer_situation(valeur,liste,joueur):
    '''
    :param valeur liste des deux coordonnées proposées par le joueur
    :param joueur entier (1 ou 2)
    :param liste contenant tous les coups possibles
    :return liste contenant grille
    '''
    if joueur == 1:
        liste[valeur[0]][valeur[1]] = 1
    else:
        liste[valeur[0]][valeur[1]] = 2
    return liste

#evalue suivant les elemetns rstants
def tester_fin(liste):
    '''
    :param liste grille de jeu
    :return booléen True (jeu fini) ou False (jeu non terminé)
    '''
    for i in range(3):
        if liste[i][0] != 0:
            if liste[i][0] == liste[i][1] == liste[i][2]:
                return True
    for i in range(3):
        if liste[0][i] != 0:
            if liste[0][i] == liste[1][i] == liste[2][i]:
                return True
    if liste[0][0] != 0:        
        if  liste[0][0] == liste[1][1] == liste[2][2]:
            return True
    if liste[0][2] != 0:
        if  liste[0][2] == liste[1][1] == liste[2][0]:
            return True
    for i in range(3):
        for j in range(3):
            if liste[i][j] == 0:
                return False
    return True
    
    
    
def fin_jeu(liste,joueur):
    '''
    :param joueur entier (1 ou 2)
    :param liste contenant la grille de jeu
    :return affichage du résultat avec print
    '''
    for i in range(3):
        if liste[i][0] == liste[i][1] == liste[i][2]:
            return print("joueur"+ str(joueur) + " a gagné")
    for i in range(3):
        if liste[0][i] == liste[1][i] == liste[2][i]:
            return print("joueur"+ str(joueur) + " a gagné")
    if  liste[0][0] == liste[1][1] == liste[2][2]:
        return print("joueur"+ str(joueur) + " a gagné")
    if  liste[0][2] == liste[1][1] == liste[2][0]:
        return print("joueur"+ str(joueur) + " a gagné")
    return print("égalité")

def coup_ordi(liste):
    alea = randint(0,len(liste))
    return liste[alea]
        
    
#evaluation pour le min_max
def evaluation_minmax(liste):
    return 100

