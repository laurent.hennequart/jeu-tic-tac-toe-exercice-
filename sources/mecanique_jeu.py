import nim as jeu
from random import randint


def initialisation():
    """
    :return: (int) 1 ou 2
    
    """
    return randint(1,2)

def message_joueur(joueur):
    """
    :param joueur: (int) 1 ou 2
    :return: (string) message avec l\'identifiant joueur
    >>> message_joueur(1)
    'A vous de jouer, joueur1 : '
    """
    return 'A vous de jouer, joueur'+ str(joueur) + ' : '

def changer_joueur(joueur):
    '''
    :param joueur: (int) 1 ou 2
    :return: (in) 1 ou 2
    >>> changer_joueur(1)
    2
    '''
    if joueur == 1:
        joueur = 2
    else:
        joueur = 1
    return joueur
    
def boucle_jeu(joueur, situation):
    """
    :param joueur: (int) 1 ou 2
    :param situation: (liste) liste décrivant l'état du jeu
    :return: rien
    :déroule toutes les étapes du jeu, modifie la liste situation et joueur  
    """
    coups_possibles = jeu.coups_possibles(situation)
    print(message_joueur(joueur))
    coup = jeu.saisie_coup(coups_possibles)
    situation = jeu.changer_situation(coup,situation,joueur)
    #jeu.tester_fin(coup,situation) 
    if jeu.tester_fin(situation):  #teste si la liste est vide ou pas
        #resultat(coup,situation)
        jeu.fin_jeu(situation,joueur)
        print(jeu.afficher_jeu(situation))
    else:
        print(jeu.afficher_jeu(situation))
        joueur = changer_joueur(joueur)
        boucle_jeu(joueur,situation)

if __name__ == '__main__':
    import doctest
    doctest.testmod()
    joueur_courant = initialisation()
    print(jeu.afficher_jeu(jeu.initialisation_liste()))
    boucle_jeu(joueur_courant,jeu.initialisation_liste())
    