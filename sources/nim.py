# coding: utf-8

#Jeu de nim (12 pièces)

from random import randint

#élément du jeu (12 éléments d'un tableau), il est envisageable de paramétrer ce nombre
def initialisation_liste():
    '''
    :return liste correspondant à la pile de jeu
    '''
    return [k for k in range(1,13)]
#affichage du jeu
def afficher_jeu(liste):
    '''
    param liste contenant les coups possibles
    return une chaine avec autant de symbôles que de coup possible
    '''
    ch=''
    for k in range(len(liste)):
        ch=ch+' | '
    return ch

#propose les coups possibles
def saisie_coup(liste):
    '''
    param liste liste contenant les différentes possibilités du joueur courant
    '''
    flag=False
    while flag==False:
        n=int(input())
        if n<=len(liste):
            flag=True
        else:
            flag=False
    return n
        

#teste si les coups proposés sont corrects
def coups_possibles(situation):
    '''
    param valeur entier proposé
    param liste contenant tous les coups possibles
    return une liste avec 1 , 1et 2,  1 et 2 et 3
    '''
    if len(situation)>2:
        return [1,2,3]
    else :
        return [k for k in range(1,len(situation)+1)]

#retourne la liste des coups possibles suivant le nombre restant
def changer_situation(valeur,liste,joueur):
    '''
    :param valeur entier proposé
    :param joueur facultatif
    :param liste contenant tous les coups possibles
    :return liste contenant valeur éléments en moins
    '''
    k=len(liste)-valeur
    return [i for i in range(1,k+1)]

#evalue suivant les elemetns rstants
def tester_fin(liste):
    '''
    :param liste des éléments restants
    :return True (jeu fini) ou False (jeu non terminé)
    '''
    if len(liste)>0:
        return False
    else:
        return True

def fin_jeu(situation,joueur):
    '''
    :param liste des éléments restants
    :return impression du résultat avec print
    '''
    return print("joueur"+ str(joueur) + " a perdu")

def coup_ordi(liste):
    '''
    :param liste des éléments restants que l'on peut retirer
    :return entier entre 1 ,  1 et 2 , et 1 et 2 et 3
    '''
    return randint(1,len(liste))
    
#evaluation pour le min_max
def evaluation_minmax(liste):
    return 100
