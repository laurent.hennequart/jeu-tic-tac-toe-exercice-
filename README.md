Auteurs : Laurent Hennequart et Stéphane Devernay

# TP JEUX A DEUX JOUEURS

L'objectif du TP est de programmer un jeu à deux joueurs (ici le Tic Tac Toe ou Morpion)
On trouvera une explication de ce jeu [ici](https://fr.wikipedia.org/wiki/Tic-tac-toe)

## Découverte du jeu de NIM

Pour comprendre une possibilité de programmation de ce type de jeu, on va étudier le jeu de Nim.
Il correspond dans sa présentation à la règle suivante :
-on retire chacun son tour 1, 2 ou 3 pièces (ou tiges ou ...)
-le perdant est le dernier à retirer des pièces 

On dispose pour cela de deux fichiers :
`nim.py` et `mecanique_jeu.py`

Le deuxième fichier sert d'interface : il regroupe le fonctionnement général d'un jeu à deux joueurs.
Le fonctionnnement spécifique du jeu Nim est détaillé dans `nim.py`.
Pour charger le jeu Nim, notez l'instruction : `import nim as jeu` et les références à `jeu.fonction()` dans `mecanique_jeu.py` sachant que fonction() se trouve dans `nim.py`.

Nous procédrons de la même manière pour le Tic Tac Toe en remplaçant `nim.py` par un fichier `ttt.py`.
On aura donc `import ttt as jeu` dans `mecanique_jeu.py`.

## Struture du jeu

Dans le même dossier que le fichier `mecanique_jeu.py`, enregistre le fichier `ttt.py`.


Dans `initialisation_jeu()`, on va placer les 9 valeurs de la grille de morpion dans un tableau à deux dimensions :

```python
def initialisation_liste():
    '''
    :return liste avec les 9 valeurs initialisées à 0
    '''
    return [[0,0,0],
            [0,0,0],
            [0,0,0]
            ]

```

La fonction `afficher_jeu(liste)` permet d'avoir un affichage concret du jeu.

```python
def afficher_jeu(liste):
    '''
    superbe affichage du jeu
    :param liste contenant les situations du jeu
    :return une grille avec les pions B et N
    '''
    for k in range(3):
        print(' ',end='')
        for i in range(3):
            print('--',end='')
        print('-')
        print(k+1,end='')
        for i in range(3):
            print('|',end='')
            if liste[k][i]==0:
                print(' ',end='')
            elif liste[k][i]==1:
                print('X',end='')
            else:
                print('O',end='')
        print('|')
    print(' ', end='')
    for i in range(3):
        print('--',end='')
    print('-')
    print('  ',end='')
    for i in range(3):
        print(i+1, end=' ')

```

## Gestion des entrées

Sachant que le joueur doit rentrer la ligne et la colonne (deux entiers entre 1 et 3), ajoute le code nécessaire dans le code suivant de la fonction `saisie_coup()` :
```python
def saisie_coup(liste):
    '''
    permet de saisir les valeurs horizontales et verticales du joueur sachant que celle-ci doit être inférieure aux dimensions de la grille
    :param liste liste contenant les différentes possibilités du joueur courant
    :return liste des deux valeurs
    '''
    flag=False
    while flag==...:
        row=int(input())
        col= ...
        if [row-1, ...] in ...:
            flag=True
        else:
            flag=False
    return [...,...]

```

Pour déterminer la liste des cases encore jouables, on utilise la fonction `coups_possibles(situation)`, complète :
```python
def coups_possibles(situation):
    '''
    :param situation contenant la grille
    :return une liste avec emplacements dispos
    '''
    coups = []
    for i in range(...):
        for j in range(...):
            if situation[i][j] == ...:
                coups.append([i,j])
    return coups
```

Complète ensuite la fonction `changer_situation(valeur,liste,joueur)` qui change la grille selon le choix du joueur :
```python
def changer_situation(valeur,liste,joueur):
    '''
    :param valeur liste des deux coordonnées proposées par le joueur
    :param joueur entier (1 ou 2)
    :param liste contenant tous les coups possibles
    :return liste contenant grille de jeu modifiée
    '''
    if joueur == ...:
        ...
    else:
        ...
    return liste

```

## Vérifier la fin du jeu

Observe la fonction `tester_fin()` :
```python
def tester_fin(liste):
    '''
    :param liste grille de jeu
    :return booléen True (jeu fini) ou False (jeu non terminé)
    '''
    for i in range(3):
        if liste[i][0] != 0:
            if liste[i][0] == liste[i][1] == liste[i][2]:
                return True
    for i in range(3):
        if liste[0][i] != 0:
            if liste[0][i] == liste[1][i] == liste[2][i]:
                return True
    if liste[0][0] != 0:        
        if  liste[0][0] == liste[1][1] == liste[2][2]:
            return True
    if liste[0][2] != 0:
        if  liste[0][2] == liste[1][1] == liste[2][0]:
            return True
    for i in range(3):
        for j in range(3):
            if liste[i][j] == 0:
                return False
    return True

```
Que permettent de vérifier les quatre premières conditions ?
Que permet de vérifier le dernier bloc de conditions ?

En extrayant une partie de ce code, complète la fonction `fin_jeu()` sachant que lorsque le jeu se termine, on soit le joueur courant gagnant, soit un match nul.

```python
def fin_jeu(liste,joueur):
    '''
    :param joueur entier (1 ou 2)
    :param liste contenant la grille de jeu
    :return affichage avec print
    '''
    ...
    ...
    ...
    ...
    ...
    ...
    ...
    ...
    ...

```

## Prolongement

Après avoir tester avec un camarade le bon fonctionnement de ton jeu, on va prolonger l'expérience en faisant jouer l'ordinateur à la place de ton camarade (tu peux le ranger).
Tu as à ta disposition toujours `nim.py` et maintenant `mecanique_jeu_humain_ordi.py`.

En vérifiant que le jeu de nim fonctionne parfaitement, reprend ton fichier `ttt.py` et ajoute ce qui est nécessaire pour que l'ordinateur joue à la place du joueur 2.
On n'oubliera pas pour l'aléatoire `from random import randint`.
